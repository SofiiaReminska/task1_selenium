package com.epam.lab.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class GoogleTest {
    private static final String BASE_URL = "http://www.google.com";
    private static final By SEARCH_INPUT_LOCATOR = By.name("q");
    private static final By IMAGE_TAB_LOCATOR = By.cssSelector(".q.qs[href*='&tbm=isch']");
    private static final By SEARCH_RESULT_IMAGES_LOCATOR = By.xpath("//*[@id='search']//*[@data-row]//img");
    private static final Logger LOG = LogManager.getLogger(GoogleTest.class);
    private static WebDriver driver;

    @BeforeMethod
    public static void before() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        LOG.info("Starting browser");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public static void findAppleGoogleTest() {
        LOG.info("Loading page {}", BASE_URL);
        driver.get(BASE_URL);

        LOG.info("Perform search");
        WebElement searchInput = driver.findElement(SEARCH_INPUT_LOCATOR);
        searchInput.sendKeys("Apple");
        searchInput.submit();

        LOG.info("Click image tab");
        driver.findElement(IMAGE_TAB_LOCATOR).click();

        LOG.info("Verifying images are visible");
        List<WebElement> resultImages = driver.findElements(SEARCH_RESULT_IMAGES_LOCATOR);
        assertFalse(resultImages.isEmpty(), "Result images list is empty");
        assertTrue(resultImages.stream().anyMatch(WebElement::isDisplayed),
                "No visible images on page");
    }

    @AfterMethod
    public void after() {
        driver.quit();
    }
}
